import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestTribalScreaming {

	//======================================
	/*
	 * Requirment1: One person is amazing
	 * 1. refactor PHASE: 
	 */
	//======================================
	
	@Test
	public void testOnePersonIsAmazing() {
		
		TribalScreaming ts = new TribalScreaming();
		String result = ts.Scream("Peter");
		assertEquals("Peter is amazing",result);
		
	}

	
	//======================================
		/*
		 * Requirment2: Nobody is listening
		 * 3. REFACTOR PHASE: Input is null, gives expected output.
		 */
		//======================================
	
	
	@Test
	 
	public void testNobodyIsListening() {
		TribalScreaming ts = new TribalScreaming();
		String result = ts.Scream("");
		assertEquals("You is amazing",result);
		}
	
	//======================================
		/*
		 * Requirment3: Peter is shouting
		 * 3. REFACTOR PHASE: By Entering Uppercase Input ,gets expected output.
		 */
		//======================================
		
	
	
		@Test
		public void testPeterIsShouting() {
		TribalScreaming ts = new TribalScreaming();
		String result = ts.Scream("PETER");
		assertEquals("PETER is amazing",result);
		}
		
	

	//======================================
		/*
		 * Requirment4: Two people is amazing...
	
		 */
		//======================================
		
		@Test
		public void testTwoPeopleAreAmazing() {
		TribalScreaming ts = new TribalScreaming();
		String[] array = {"Albert","Pritesh"};
		String result = ts.Scream2(array);
		assertEquals("Albert and Pritesh are amazing",result);
		}
		
		//======================================
		/*
		* Requirment5: More than Two people are amazing... 
	    */
	   //======================================
				
		@Test
		public void testMoreThanTwoPeopleAreAmazing() {
			TribalScreaming ts = new TribalScreaming();	
			String[] array = {"Albert","Pritesh","Jigisha"};
			String result = ts.Scream1(array);
			assertEquals("Albert,Pritesh, and Jigisha are amazing",result);
			
		}
		
		//======================================
				/*
				* Requirment6: Shouting a lot people... 
			    */
			   //======================================
		@Test
		public void testShoutaLotPeople() {
			TribalScreaming ts = new TribalScreaming();
			String[] array = {"Peter","Jigisha"};
			String result = ts.Scream3(array);
			assertEquals("Peter is amazing. JIGESHA ALSO",result);
			
		}
		
	
}
